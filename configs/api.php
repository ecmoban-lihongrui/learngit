    <?php
defined('IN_ECJIA') or exit('No permission resources.');

return array(
    //address
    'address/add'           => 'user::address/add',
    'address/delete'        => 'user::address/delete',
    'address/info'          => 'user::address/info',
    'address/list'          => 'user::address/list',
    'address/setDefault'    => 'user::address/setDefault',
    'address/update'        => 'user::address/update',
    
    //article
    'article/category'      => 'api::article/category',//2.4+api(测试)
    'article/detail'        => 'article::article/detail',
	//article 1.0
    'article'				=> 'article::article/detail',
		
    //cart
    'cart/create'           => 'cart::cart/create',
    'cart/gift/create'      => 'api::cart/gift/create',//2.4+api(测试)
    'cart/delete'           => 'cart::cart/delete',
    'cart/list'             => 'cart::cart/list',
    'cart/update'           => 'cart::cart/update',
    'cart/buyagain'         => 'cart::cart/buyagain',// 1.13add
    'flow/checkOrder'       => 'cart::flow/checkOrder',
    'flow/done'             => 'cart::flow/done',
    
    //comments
	'comment/create'        => 'comment::goods/create',
    
    //feedback
    'feedback/list'         => 'feedback::feedback/list',
    'feedback/create'       => 'feedback::feedback/create',
		
    //goods
    'goods/category'        => 'goods::goods/category',
	'goods/list'        	=> 'goods::goods/list',
	'goods/suggestlist'     => 'goods::goods/suggestlist',
	'goods/groupbuygoods'   => 'goods::goods/groupbuygoods',
    'goods/comments'        => 'comment::goods/comments',
    'goods/detail'          => 'goods::goods/detail',
    'goods/desc'            => 'goods::goods/desc',
    'goods/brand'           => 'goods::goods/brand',
    'goods/price_range'     => 'goods::goods/price_range',
    'search'                => 'goods::search',
    'searchKeywords'        => 'goods::searchKeywords',
    //goods 1.0
    'brand'					=> 'goods::goods/brand',
	'category'              => 'goods::goods/category',
	'comments'				=> 'comment::goods/comments',
	//'goods'					=> 'goods::goods/detail',
	'price_range'			=> 'goods::goods/price_range',
		
    //home
    'home/category'         => 'goods::home/category',
    'home/data'             => 'mobile::home/data',
	'home/adsense'          => 'adsense::home/adsense',
	'home/discover'         => 'mobile::home/discover',
	'home/news'         	=> 'mobile::home/news',
	//home
    
    //order
    'order/affirmReceived'  => 'orders::order/affirmReceived',
    'order/cancel'          => 'orders::order/cancel',
    'order/list'            => 'orders::order/list',
    'order/pay'             => 'orders::order/pay',
    'order/detail'          => 'orders::order/detail',
    'order/update'          => 'orders::order/update',
    'order/express'         => 'orders::order/express',
    
    //shop
    'shop/config'           => 'setting::shop/config',
    'shop/server'           => 'setting::shop/server',
    'shop/region'           => 'setting::shop/region',
    'shop/payment'          => 'payment::shop/payment',
    'shop/help'             => 'article::shop/help',
    'shop/help/detail'      => 'article::shop/help/detail',
    //shop 1.0
	'config'           		=> 'system::shop/config',
	'shopHelp'				=> 'article::shop/help',
	'region'           		=> 'system::shop/region',
		
		
    //user
    'user/collect/create'   => 'user::user/collect/create',
    'user/collect/delete'   => 'user::user/collect/delete',
    'user/collect/list'     => 'user::user/collect/list',
    'user/info'             => 'user::user/info',
    'user/signin'           => 'user::user/signin',
	'user/signout'          => 'user::user/signout',
    'user/signup'           => 'user::user/signup',
	'user/update'           => 'user::user/update',
    'user/password'         => 'user::user/password',
    'user/signupFields'     => 'user::user/signupFields',
    'user/account/record'   => 'user::user/account/record',
    'user/account/log'      => 'user::user/account/log',
    'user/account/deposit'  => 'user::user/account/deposit',
    'user/account/pay'      => 'user::user/account/pay',
    'user/account/raply'    => 'user::user/account/raply',
    'user/account/cancel'   => 'user::user/account/cancel',
    'validate/bonus'        => 'user::validate/bonus',
    'validate/integral'     => 'user::validate/integral',
    
	'user/connect/signin'	=> 'user::user/connect/signin',
	'user/connect/signup'	=> 'user::user/connect/signup',
		
	//多商铺
	'seller/category'		=> 'seller::seller/category',
	'seller/list'			=> 'seller::seller/list',
	'seller/search'			=> 'seller::seller/search',
	'seller/collect/list'	=> 'seller::seller/collect/list',
	'seller/collect/create'	=> 'seller::seller/collect/create',
	'seller/collect/delete'	=> 'seller::seller/collect/delete',
	'merchant/config'		=> 'seller::merchant/config',
	'merchant/home/data'	=> 'seller::merchant/home/data',
	'merchant/goods/category'	=> 'seller::merchant/goods/category',
	'merchant/goods/list'		=> 'seller::merchant/goods/list',
	'merchant/goods/suggestlist' => 'seller::merchant/goods/suggestlist',
	'goods/search'				=> 'goods::goods/search',
		
		
	//手机注册
	'user/userbind'     	=> 'user::user/userbind',
	'validate/bind'         => 'user::validate/bind',
	//第三方登录
	'user/snsbind'           => 'user::user/snsbind',
	
	//ecjia
	'admin/orders/list'			=> 'orders::admin/orders/list',
	'admin/orders/detail'		=> 'orders::admin/orders/detail',
	'admin/orders/cancel'		=> 'orders::admin/orders/cancel',
	
	
	'admin/goods/list'			=> 'goods::admin/goods/list',
	'admin/goods/detail'		=> 'goods::admin/goods/detail',
	'admin/goods/togglesale'	=> 'goods::admin/goods/togglesale',
	'admin/goods/trash'			=> 'goods::admin/goods/trash',
	'admin/goods/desc'			=> 'goods::admin/goods/desc',
	'admin/goods/product_search' => 'goods::admin/goods/product_search',
		
		
	'admin/user/signin'			=> 'user::admin/user/signin',
	'admin/user/userinfo'		=> 'user::admin/user/userinfo',
	'admin/user/forget_request'	=> 'user::admin/user/forget_request',
	'admin/user/forget_validate' => 'user::admin/user/forget_validate',
	'admin/user/password' 		=> 'user::admin/user/password',
	'admin/user/search' 		=> 'user::admin/user/search',
	
	'admin/home/data'			=> 'mobile::admin/home/data',
	'admin/shop/config'			=> 'setting::admin/shop/config',
		
	'admin/goods/category'		=> 'goods::admin/goods/category',
	'admin/goods/updatePrice'	=> 'goods::admin/goods/updateprice',
	'admin/merchant/info'		=> 'merchant::admin/merchant/info',
	'admin/merchant/update'		=> 'merchant::admin/merchant/update',
	//mobile
	'device/setDeviceToken' => 'mobile::device/setDeviceToken',
		
		
	'admin/connect/validate'	=> 'connect::admin/connect/validate',
	'admin/connect/signin'		=> 'connect::admin/connect/signin',
	
	'admin/flow/checkOrder'		=> 'cart::admin/flow/checkOrder',
	'admin/flow/done'			=> 'cart::admin/flow/done',
	'admin/goods/product_search' => 'goods::admin/goods/product_search',
	'admin/user/search'			=> 'user::admin/user/search',
	
	'admin/stats/orders'		=> 'orders::admin/stats/orders',
	'admin/stats/sales'			=> 'orders::admin/stats/sales',
	'admin/stats/sales_details'	=> 'orders::admin/stats/salesdetails',
	'admin/stats/visitor'		=> 'user::admin/stats/visitor',
	'admin/stats/order_sales'	=> 'orders::admin/stats/order_sales',
		
		
	'admin/order/split'			=> 'orders::admin/orders/split',
	'admin/order/receive'		=> 'orders::admin/orders/receive',
	'admin/order/update'		=> 'orders::admin/orders/update',
		
	
		
	'admin/order/payConfirm'	=> 'orders::admin/orders/payConfirm',	//收银台支付验证
	'admin/order/refundConfirm'	=> 'orders::admin/orders/refundConfirm',	//收银台退款验证
	'admin/order/check'			=> 'orders::admin/orders/check',	//收银台验单
    'admin/order/pay'           => 'orders::admin/orders/pay',//收银台付款
		
	
	/* 消息*/
	'admin/message'				=> 'mobile::admin/message',
		
	'shop/token'           		=> 'setting::shop/token',
		
	'user/forget_password'      => 'user::user/forget_password',
	'validate/forget_password'  => 'user::validate/forget_password',
	'user/reset_password'       => 'user::user/reset_password',

	'goods/mobilebuygoods'		=> 'goods::goods/mobilebuygoods',
	
	'seller/home/data'			=> 'seller::home/data',
		
	
	//专题功能
	'topic/list'				=> 'topic::topic/list',
	'topic/info'				=> 'topic::topic/info',
	//扫码登录
	'mobile/qrcode/create'				=> 'mobile::qrcode/create',
	'mobile/qrcode/bind'				=> 'mobile::qrcode/bind',
	'mobile/qrcode/validate'			=> 'mobile::qrcode/validate',
		
	//后台咨询功能
	'admin/feedback/list'				=> 'feedback::admin/feedback/list',
	'admin/feedback/messages'			=> 'feedback::admin/feedback/messages',
	'admin/feedback/reply'				=> 'feedback::admin/feedback/reply',
		
	/*掌柜1.5*/
	'admin/user/rank'           => 'user::admin/user/rank',
	'admin/favourable/list'		=> 'favourable::admin/favourable/list',
	'admin/favourable/add'		=> 'favourable::admin/favourable/manage',
	'admin/favourable/update'	=> 'favourable::admin/favourable/manage',
	'admin/favourable/info'		=> 'favourable::admin/favourable/info',
	'admin/favourable/delete'	=> 'favourable::admin/favourable/delete',
	'admin/goods/brand'			=> 'goods::admin/goods/brand',
	'admin/user/signout'		=> 'user::admin/user/signout',
		
	'admin/promotion/list'		=> 'promotion::admin/promotion/list',
	'admin/promotion/detail'	=> 'promotion::admin/promotion/detail',
	'admin/promotion/delete'	=> 'promotion::admin/promotion/delete',
	'admin/promotion/add'	    => 'promotion::admin/promotion/manage',
	'admin/promotion/update'	=> 'promotion::admin/promotion/manage',

	/* 收银台1.5*/
	'admin/order/quickpay'		=> 'orders::admin/orders/quickpay',
	'admin/stats/payment' 		=> 'orders::admin/stats/payment',
	'admin/order/list'			=> 'orders::admin/orders/list',
	
		
	/* 收银台1.6*/
	'admin/user/info'			=> 'user::admin/user/info',
	'admin/bonus/validate'		=> 'cart::admin/bonus/validate',
		
		
	/* app1.7*/
	'invite/user'				=> 'affiliate::invite/user',
	'invite/reward'				=> 'affiliate::invite/reward',
	'invite/record'				=> 'affiliate::invite/record',
	'invite/validate'			=> 'affiliate::invite/validate',
	
	'connect/signin'			=> 'connect::connect/signin',
	'connect/signup'			=> 'connect::connect/signup',
	'connect/bind'				=> 'connect::connect/bind',
		
	'shop/info'             	=> 'article::shop/info',
	'shop/info/detail'			=> 'article::shop/info/detail',
	
	'user/bonus'             	=> 'user::user/bonus',
		
	'bonus/validate'			=> 'bonus::bonus/validate',		//验证红包
	'bonus/bind'				=> 'bonus::bonus/bind',			//兑换红包需要登录
	
	'mobile/hot_keywords'		=> 'mobile::home/hot_keywords',
		
	'orders/comment'			=> 'orders::order/comment',
	'orders/comment/detail'		=> 'orders::order/comment/detail',
	'comment/create'        	=> 'comment::goods/create',
	'goods/comment/list'        => 'comment::goods/comment_list',
		
	/*1.9*/
	'mobile/shake'				=> 'mobile::activity/shake',
	'goods/stock'				=> 'goods::goods/stock',
		
	'order/return/list'			=> 'orders::order/return/list',
	'order/return/detail'		=> 'orders::order/return/detail',
	'order/return/apply'		=> 'orders::order/return/apply',
	'order/return/cancel'		=> 'orders::order/return/cancel',
	'order/return/reason'		=> 'orders::order/return/reason',
		
	/*1.10*/
	'mobile/checkin'			=> 'mobile::checkin/integral',
	'mobile/checkin/record'		=> 'mobile::checkin/record',
	'mobile/toutiao'        	=> 'mobile::mobile/toutiao',
		
	'goods/filter'          	=> 'goods::goods/filter',
		
	/* 掌柜1.7*/
	'admin/order/return/list'	=> 'orders::admin/orders/return/list',
	'admin/order/return/detail'	=> 'orders::admin/orders/return/detail',
	'admin/order/consignee/list' => 'orders::admin/orders/consignee/list',
	'admin/order/operate/consignee' => 'orders::admin/orders/operate/consignee',
	'admin/order/operate/money' => 'orders::admin/orders/operate/money',
	'admin/order/operate/shipping/detail' => 'orders::admin/orders/operate/shipping_detail',
	'admin/order/operate/pay' 	=> 'orders::admin/orders/operate/pay',
	'admin/order/operate/cancel'	=> 'orders::admin/orders/operate/cancel',
	'admin/order/operate/shipping'	=> 'orders::admin/orders/operate/shipping',
	'admin/order/operate/delivery'	=> 'orders::admin/orders/operate/delivery',
	'admin/order/operate/setgrab'	=> 'orders::admin/orders/operate/setgrab',
	'admin/order/operate/cancelgrab'	=> 'orders::admin/orders/operate/cancelgrab',
		
	'admin/order/shipping/list'	=> 'orders::admin/orders/shipping/list',
		
	'admin/order/return/operate/agree'		=> 'orders::admin/orders/return/operate/agree',
	'admin/order/return/operate/receive'	=> 'orders::admin/orders/return/operate/receive',
	'admin/order/return/operate/refund'		=> 'orders::admin/orders/return/operate/refund',
	'admin/order/return/operate/delivery'	=> 'orders::admin/orders/return/operate/delivery',
	'admin/order/return/operate/finished'	=> 'orders::admin/orders/return/operate/finished',
	'admin/order/express'					=> 'orders::admin/orders/express',
	'admin/order/store/list'				=> 'orders::admin/orders/store/list',
		
	/* 掌柜1.8新增*/
	'admin/merchant/category'				=> 'merchant::admin/merchant/category',
	'admin/goods/restore'					=> 'goods::admin/goods/restore',
	'admin/goods/add'						=> 'goods::admin/goods/add',
    'admin/goods/update'					=> 'goods::admin/goods/update',
	'admin/goods/toggle/suggest'			=> 'goods::admin/goods/toggle/suggest',
	'admin/goods/toggle/sale'				=> 'goods::admin/goods/toggle/sale',
	'admin/goods/toggle/gifts'				=> 'goods::admin/goods/toggle/gifts',
	'admin/goods/toggle/free_shipping'		=> 'goods::admin/goods/toggle/free_shipping',
	'admin/goods/trash/list'				=> 'goods::admin/goods/trash/list',
    'admin/goods/update/desc'	            => 'goods::admin/goods/updatedesc',

	'admin/goods/promote/add'				=> 'goods::admin/goods/promote/add',
	'admin/goods/promote/update'			=> 'goods::admin/goods/promote/update',
	'admin/goods/promote/delete'			=> 'goods::admin/goods/promote/delete',

	'admin/goods/category/add'				=> 'goods::admin/goods/category/add',
	'admin/goods/category/detail'			=> 'goods::admin/goods/category/detail',
	'admin/goods/category/update'			=> 'goods::admin/goods/category/update',
	'admin/goods/category/show'				=> 'goods::admin/goods/category/show',
	'admin/goods/category/delete'			=> 'goods::admin/goods/category/delete',
		
	'admin/goods/gallery/add'				=> 'goods::admin/goods/gallery/add',
	'admin/goods/gallery/delete'			=> 'goods::admin/goods/gallery/delete',
	'admin/goods/move/category'				=> 'goods::admin/goods/move/category',
		
	'admin/goods/merchant/category/list'	=> 'goods::admin/goods/merchant/category/list',
	'admin/goods/merchant/category/add'		=> 'goods::admin/goods/merchant/category/add',
	'admin/goods/merchant/category/detail'	=> 'goods::admin/goods/merchant/category/detail',
	'admin/goods/merchant/category/update'	=> 'goods::admin/goods/merchant/category/update',
	'admin/goods/merchant/category/show'	=> 'goods::admin/goods/merchant/category/show',
	'admin/goods/merchant/category/delete'	=> 'goods::admin/goods/merchant/category/delete',
		
	'admin/shop/attach/add'	=> 'mobile::admin/shop/attach/add',
    
    //收银台新增
    'admin/shop/captcha/sms'        => 'captcha::captcha/admin/sms',
    'admin/shop/captcha/validate/sms'   => 'captcha::captcha/admin/validate/sms',
		
	//1.11.0新增
	'user/bind'             	=> 'user::user/bind',    				//用户绑定邮箱或手机
	'shop/captcha/sms'        	=> 'captcha::captcha/sms', 				//用户绑定手机时获取验证码
	'shop/captcha/mail'        	=> 'captcha::captcha/mail', 			//用户绑定邮箱时获取验证码
	'goods/spike/period'		=> 'goods::spike/period',				//秒杀活动时间段
	'goods/spike/goodslist'		=> 'goods::spike/goodslist',			//某一秒杀时间段下的商品
	
	//掌柜1.9新增
	'admin/user/update'					=> 'user::admin/user/merchant/update',   		//修改管理员基本信息
	'admin/shop/captcha/sms'			=> 'captcha::captcha/admin/shop/sms',    		//管理员绑定手机号时发送验证码
	'admin/shop/captcha/mail'			=> 'captcha::captcha/admin/shop/mail',   		//管理员绑定邮箱时发送验证码
	'admin/user/bind'					=> 'user::admin/user/merchant/bind',     		//管理员去绑定邮箱或手机
	'admin/goods/gallery/sort'			=> 'goods::admin/goods/gallery/sort',	 		//商品相册排序
	'admin/goods/gallery/delete/batch'  => 'goods::admin/goods/gallery/delete_batch',	//批量删除商品相册
	'admin/orders/today'				=> 'orders::admin/orders/today',      			//掌柜今日订单列表
	
	//收银POS机新增
	'admin/order/eticket/check'			=> 'orders::admin/orders/eticket/check',		//验单信息
	
		
	//多商户1.12
	'user/account/realname/verify'		=> 'user::user/account/verify',			//用户实名认证申请，修改
	'order/return/activation'			=> 'orders::order/return/activation',	//激活退换货订单
	
    //掌柜新增2017-12-20
    'admin/goods/product' => 'goods::admin/goods/product/list',//（商品货品列表）
    'admin/goods/product/add' => 'goods::admin/goods/product/add',//（商品货品添加）
    'admin/goods/product/update' => 'goods::admin/goods/product/update',//（商品货品更新）
    'admin/goods/product/detail' => 'goods::admin/goods/product/detail',//（商品货品详情）
    'admin/goods/attr' => 'goods::admin/goods/attr',//（商品属性列表）
    
	//2018-02-02 	
	'captcha/image' 			=> 'captcha::captcha/image', //增加验证码图片
	
    //贡云接口
    //     'cart/create'           => 'gongyun::cart/create',
    //     'cart/update'           => 'gongyun::cart/update',
    //     'flow/done'             => 'gongyun::flow/done',
    //     'goods/stock'				=> 'gongyun::goods/stock',
    //     'order/return/detail'		=> 'gongyun::order/return/detail',
    //     'order/return/apply'		=> 'gongyun::order/return/apply',
    
	//多商户1.13
	'user/coupons' 				=> 'user::user/coupons',    				//1.13获取用户优惠券列表
	'coupon/available/coupons' 	=> 'coupon::coupon/available/coupons', 		//1.13获取可领取优惠券列表
	'coupon/user/receive'   	=> 'coupon::coupon/user/receive',   		//1.13用户领取优惠券
	'coupon/contain/goods'   	=> 'coupon::coupon/contain/goods',   		//1.13优惠券指定的商品
	'shop/keywords/suggest'		=> 'seller::shop/keywords/suggest',   		//1.13搜索推荐关键字
	'order/return/goodsinfo'	=> 'orders::order/return/goodsinfo',		//1.13获取订单退换货商品信息
	
	//多商户掌柜1.10
	'admin/goods/product/list'  				=> 'goods::admin/goods/product/list',  					//掌柜1.10（商品货品列表）
	'admin/merchant/shipping/template/list'		=> 'merchant::admin/merchant/shipping/template/list',	//掌柜1.10 掌柜获取某个商家运费模板列表
	'admin/goods/shipping/setting'				=> 'goods::admin/goods/shipping/setting',				//掌柜1.10 掌柜设置商品运费
	'admin/coupon/add'                          => 'coupon::admin/coupon/add',                          //掌柜1.10添加优惠券
	'admin/coupon/detail'                       => 'coupon::admin/coupon/detail',                       //掌柜1.10获取优惠券详情
	'admin/coupon/goods/category'               => 'coupon::admin/coupon/goods/category',               //掌柜1.10优惠券可选的商品分类
	'admin/area/list'               			=> 'setting::admin/area/list',               			//掌柜1.10掌柜获取区域列表
	'admin/coupon/update'               		=> 'coupon::admin/coupon/update',               		//掌柜1.10掌柜编辑优惠券
	'admin/coupon/delete'               		=> 'coupon::admin/coupon/delete',               		//掌柜1.10掌柜删除优惠券
	'admin/coupon/list'               			=> 'coupon::admin/coupon/list',               			//掌柜1.10掌柜获取优惠券列表
);

// end