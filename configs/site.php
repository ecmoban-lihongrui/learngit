<?php
defined('IN_ECJIA') or exit('No permission resources.');
return array(
    // 自定义站点地址
    'custom_home_url'          	=> 'https://b2b2c.ecjia.com',
	'shop_type'					=> 'b2b2c',
    'main_app'                  => 'dscapi',
    
    'gongyun_apiurl'            => 'http://api.test.jioao.cn/gy_api2/',//贡云api地址
    //请求url，正式环境地址：http://api.biz.jioao.cn/gy_api    测试环境地址1：http://api.test.jioao.cn/gy_api 测试环境地址2：http://api.test.jioao.cn/gy_api2/
    
    //国际区号，国际短信使用
//     'international_area_code' => '60',
);

// end
