<?php
defined('IN_ECJIA') or exit('No permission resources.');
/**
 * 系统APPS配置表
 * APP文件夹映射配置
 * ROUTE_M => appId:floderName
 */
return array(
    /*收银台导购管理*/
    'achievement' => 'achievement', 
      /*广告管理*/
    'adsense' => 'adsense',
      //推荐分成
    'affiliate' => 'affiliate',
    /* 主页 */
	  'api' => 'api',	
      /* 文章 */
    'article' => 'article', 
      /* 红包 */
    'bonus' => 'bonus', 
      /* 验证码 */
    'captcha' => 'captcha', 
      /* 购物车 */
    'cart' => 'cart',
      /* 评论管理 */
    'comment' => 'comment',
       //帐号连接
    'connect' => 'connect',
     /* 优惠券 */
    'coupon' => 'coupon',
    /* 轮播图 */
    'cycleimage' => 'cycleimage', 
    /* dscapi */
    'dscapi' => 'dscapi',     
    /* 优惠活动 */
    'favourable' => 'favourable',   
    /* 留言反馈 */
    'feedback' => 'feedback',
      /*贡云*/
    'gongyun' => 'gongyun',
      /* 商品 */
    'goods' => 'goods',
    /* 团购 */
    'groupbuy' => 'groupbuy',
    /* 邮件管理 */
    'mail' => 'mail',
     /* 商户 */
    'merchant' => 'merchant',
    //移动应用
    'mobile' => 'mobile',
    /* 订单 */
    'orders' => 'orders',
    /* 支付方式 */
    'payment' => 'payment', 
    /* 促销 */
    'promotion' => 'promotion',
     //消息推送
    'push' => 'push',
    //seller
    'seller' => 'seller',
    /*设置*/
    'setting' => 'setting',
    /* 配送方式 */
    'shipping' => 'shipping',
      //短信
    'sms' => 'sms',
     /* 报表统计 */
    'stats' => 'stats',
    /* 专题 */
    'topic' => 'topic',
    /* 会员 */
	  'user' => 'user',	
    /*仓库管理*/
    'warehouse' => 'warehouse',		
);

// end