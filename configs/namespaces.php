<?php

$contentDir = realpath(__DIR__ . '/../');

return array(
    'Ecjia\App\Achievement'     => $contentDir . '/apps/achievement/classes',
	'Ecjia\App\Adsense'         => $contentDir . '/apps/adsense/classes',
    'Ecjia\App\Affiliate'       => $contentDir . '/apps/affiliate/classes',
    'Ecjia\App\Api'             => $contentDir . '/apps/api/classes',
    'Ecjia\App\Article'         => $contentDir . '/apps/article/classes',
    'Ecjia\App\Bonus'           => $contentDir . '/apps/bonus/classes',
    'Ecjia\App\Captcha'         => $contentDir . '/apps/captcha/classes',
    'Ecjia\App\Cart'            => $contentDir . '/apps/cart/classes',
    'Ecjia\App\Comment'         => $contentDir . '/apps/comment/classes',
    'Ecjia\App\Connect'         => $contentDir . '/apps/connect/classes',
    'Ecjia\App\Coupon'          => $contentDir . '/apps/coupon/classes',
    'Ecjia\App\Cycleimage'      => $contentDir . '/apps/cycleimage/classes',
    'Ecjia\App\Dscapi'          => $contentDir . '/apps/dscapi/classes',
    'Ecjia\App\Favourable'      => $contentDir . '/apps/favourable/classes',
    'Ecjia\App\Feedback'        => $contentDir . '/apps/feedback/classes',
    'Ecjia\App\Gongyun'         => $contentDir . '/apps/gongyun/classes',
    'Ecjia\App\Goods'           => $contentDir . '/apps/goods/classes',
    'Ecjia\App\Groupbuy'        => $contentDir . '/apps/groupbuy/classes',
    'Ecjia\App\Mail'            => $contentDir . '/apps/mail/classes',
    'Ecjia\App\Merchant'        => $contentDir . '/apps/merchant/classes',
    'Ecjia\App\Mobile'          => $contentDir . '/apps/mobile/classes',
    'Ecjia\App\Orders'          => $contentDir . '/apps/orders/classes',
    'Ecjia\App\Payment'         => $contentDir . '/apps/payment/classes',
    'Ecjia\App\Promotion'       => $contentDir . '/apps/promotion/classes',
    'Ecjia\App\Push'            => $contentDir . '/apps/push/classes',
    'Ecjia\App\Seller'          => $contentDir . '/apps/seller/classes',
    'Ecjia\App\Setting'         => $contentDir . '/apps/setting/classes',
    'Ecjia\App\Shipping'        => $contentDir . '/apps/shipping/classes',
    'Ecjia\App\Sms'             => $contentDir . '/apps/sms/classes',
    'Ecjia\App\Stats'           => $contentDir . '/apps/stats/classes',
	'Ecjia\App\Topic'           => $contentDir . '/apps/topic/classes',
    'Ecjia\App\User'            => $contentDir . '/apps/user/classes',
    'Ecjia\App\Warehouse'       => $contentDir . '/apps/warehouse/classes', 
);

//end