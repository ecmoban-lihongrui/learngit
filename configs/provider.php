<?php

defined('IN_ECJIA') or exit('No permission resources.');

return array(

	'Royalcms\Component\Agent\AgentServiceProvider',
    'Royalcms\Component\Excel\ExcelServiceProvider',
    'Royalcms\Component\Memcache\MemcacheServiceProvider',
    'Royalcms\Component\XmlResponse\XmlResponseServiceProvider',
    'Royalcms\Component\UEditor\UEditorServiceProvider',
    'Royalcms\Component\QrCode\QrCodeServiceProvider',
    'Royalcms\Component\Repository\RepositoryServiceProvider',
    'Royalcms\Component\Sms\SmsServiceProvider',
    
    'Ecjia\System\Providers\SystemServiceProvider',
     
    'Ecjia\App\Achievement\AchievementServiceProvider',
    'Ecjia\App\Adsense\AdsenseServiceProvider',
    'Ecjia\App\Affiliate\AffiliateServiceProvider',
    'Ecjia\App\Api\ApiServiceProvider',
    'Ecjia\App\Article\ArticleServiceProvider',
    'Ecjia\App\Bonus\BonusServiceProvider',
    'Ecjia\App\Captcha\CaptchaServiceProvider',
    'Ecjia\App\Cart\CartServiceProvider',
    'Ecjia\App\Comment\CommentServiceProvider',
    'Ecjia\App\Connect\ConnectServiceProvider',
    'Ecjia\App\Coupon\CouponServiceProvider',
    'Ecjia\App\Cycleimage\CycleimageServiceProvider',
    'Ecjia\App\Dscapi\DscapiServiceProvider',
    'Ecjia\App\Favourable\FavourableServiceProvider',
    'Ecjia\App\Feedback\FeedbackServiceProvider',
    'Ecjia\App\Gongyun\GongyunServiceProvider',
    'Ecjia\App\Goods\GoodsServiceProvider',
    'Ecjia\App\Groupbuy\GroupbuyServiceProvider',
    'Ecjia\App\Mail\MailServiceProvider',
    'Ecjia\App\Merchant\MerchantServiceProvider',
    'Ecjia\App\Mobile\MobileServiceProvider',
    'Ecjia\App\Orders\OrdersServiceProvider',
    'Ecjia\App\Payment\PaymentServiceProvider',
    'Ecjia\App\Promotion\PromotionServiceProvider',
    'Ecjia\App\Push\PushServiceProvider',
    'Ecjia\App\Seller\SellerServiceProvider',
    'Ecjia\App\Setting\SettingServiceProvider',
    'Ecjia\App\Shipping\ShippingServiceProvider',
    'Ecjia\App\Sms\SmsServiceProvider',
    'Ecjia\App\Stats\StatsServiceProvider',
    'Ecjia\App\Topic\TopicServiceProvider',
    'Ecjia\App\User\UserServiceProvider',
    'Ecjia\App\Warehouse\WarehouseServiceProvider',
);

//end